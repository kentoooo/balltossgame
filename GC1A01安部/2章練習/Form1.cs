﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlCheck
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //チェックボックス1の状態を表示
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            label2.Text = "チェックボックス："+checkBox1.Checked;
        }

        //ラジオボタン1の状態を表示
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            label3.Text = "ラジオボタン1：" + radioButton1.Checked;
        }

        //ラジオボタン2の状態を表示
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            label4.Text = "ラジオボタン2" +radioButton2.Checked;
        }

        //数値を表示
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            label5.Text = "数値："+ numericUpDown1.Value;
        }

        //開始時に状態を初期化
        private void Form1_Load(object sender, EventArgs e)
        {
            label2.Text = "チェックボックス：" + checkBox1.Checked;
            label3.Text = "ラジオボタン1：" + radioButton1.Checked;
            label4.Text = "ラジオボタン2：" + radioButton2.Checked;
            label5.Text = "数値：" + numericUpDown1.Value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GradeCheck
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

      

        private void Form1_Load(object sender, EventArgs e)
        {
            labelResult_M.Text = "";
            labelResult_P.Text = "";
            labelResult_E.Text = "";
            labelConmpAvg_M.Text = "";
            labelConmpAvg_P.Text = "";
            labelConmpAvg_E.Text = "";
        }


        /// <summary>
        /// 文字列を小数に変換
        /// </summary>
        /// <param name="text">変換したい文字列</param>
        /// <param name="val">変更後の小数</param>
        private void TextToValue(string text, out double val)
        {
            //文字列をdouble型に変換
            //もし失敗したらfalseが返ってくっる
            if (double.TryParse(text, out val) == false)
            {
                //数値にできなかったとき
                //仕方ないので‐1にする
                val = -1.0f;
            }
        }

        /// <summary>
        /// 文字列を小数に変換
        /// </summary>
        /// <param name="text">変換したい文字列</param>
        /// <param name="val">変更後の整数値</param>
        private void TextToValue(string text, out int val)
        {
            //文字列をdoubl型に変換
            //もし失敗したらfalseが返ってくっる
            if (int.TryParse(text, out val) == false)
            {
                //数値にできなかったとき
                //仕方ないので‐1にする
                val = -1;
            }
        }



        /// <summary>
        /// 出席率と得点で判定する
        /// </summary>
        /// <param name="attendance">出席率</param>
        /// <param name="score">得点</param>
        /// <returns>判定結果</returns>
        private string ScoreJudge(double attendance, int score)
        {
            string result;

            //入力された値がこの範囲を超えたら
            if (attendance < 0.0 || attendance > 100.0 || score < 0 || score > 100)
            {
                //超えた場合の処理
                result = "エラー";
            }

            //出席率が80％以上
            else if (attendance >= 80.0)
            {
                //得点が80点以上
                if (score >= 80.0)
                    result = "A判定";
                //得点が70点以上
                else if (score >= 70)
                    result = "B判定";
                //得点が60点以上
                else if (score >= 60)
                    result = "C判定";
                //得点が60点にも満たない場合
                else
                    result = "不合格";
            }
            //出席率が80％にも満たなかった場合
            else
                result = "不合格";

            return result;
          
        }

        private string AverageJudge(int score, string subject)
        {
            string result;
            int average;

            //各教科の平均点を入力
            const int MathAverage = 73;
            const int PhysicalAverage = 65;
            const int EnglishAverage = 77;

            if (score < 0 || score > 100)
                return "エラー";

            //受け取った文字列が下のどれと一致してるのか
            switch (subject)
            {
                //数学という文字に一致した場合
                case "数学":
                    average = MathAverage;
                    break;
                //物理という文字に一致した場合
                case "物理":
                    average = PhysicalAverage;
                    break;
                //英語という文字に一致した場合
                case "英語":
                    average = EnglishAverage;
                    break;
                //どれとも一致しなかった場合
                default:
                    return "エラー";
            }

            //得点が平均点より高かった場合
            if (score >= average)
                result = "平均点以上";
            //得点が平均点より下回った場合
            else
                result = "平均点未満";
            return result;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //出席率を浮動小数点（double）に変換
            double attendanceM, attendanceP, attendanceE;
            TextToValue(textBox_M.Text, out attendanceM);
            TextToValue(textBox_P.Text, out attendanceP);
            TextToValue(textBox_E.Text, out attendanceE);

            //得点を整数値（int）に変換
            int scoreM, scoreP, scoreE;
            TextToValue(textBoxScore_M.Text, out scoreM);
            TextToValue(textBoxScore_P.Text, out scoreP);
            TextToValue(textBoxScore_E.Text, out scoreE);

            //成績を判定する
            labelResult_M.Text = ScoreJudge(attendanceM, scoreM);
            labelResult_P.Text = ScoreJudge(attendanceP, scoreP);
            labelResult_E.Text = ScoreJudge(attendanceE, scoreE);

            //平均値以上か未満か判定する
            labelConmpAvg_M.Text = AverageJudge(scoreM, "数学");
            labelConmpAvg_P.Text = AverageJudge(scoreP, "物理");
            labelConmpAvg_E.Text = AverageJudge(scoreE, "英語");
        }


        //リセットボタン
        private void button2_Click(object sender, EventArgs e)
        {
            textBox_M.Text = "0.0";
            textBox_P.Text = "0.0";
            textBox_E.Text = "0.0";
            textBoxScore_M.Text = "0";
            textBoxScore_P.Text = "0";
            textBoxScore_E.Text = "0";
            labelResult_M.Text = "";
            labelResult_P.Text = "";
            labelResult_E.Text = "";
            labelConmpAvg_M.Text = "";
            labelConmpAvg_P.Text = "";
            labelConmpAvg_E.Text = "";

        }
    }

}

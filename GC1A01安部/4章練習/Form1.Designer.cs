﻿namespace GradeCheck
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_M = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxScore_M = new System.Windows.Forms.TextBox();
            this.textBoxScore_P = new System.Windows.Forms.TextBox();
            this.textBoxScore_E = new System.Windows.Forms.TextBox();
            this.textBox_E = new System.Windows.Forms.TextBox();
            this.textBox_P = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.labelResult_M = new System.Windows.Forms.Label();
            this.labelResult_P = new System.Windows.Forms.Label();
            this.labelResult_E = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.labelConmpAvg_M = new System.Windows.Forms.Label();
            this.labelConmpAvg_P = new System.Windows.Forms.Label();
            this.labelConmpAvg_E = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox_M
            // 
            this.textBox_M.Location = new System.Drawing.Point(96, 77);
            this.textBox_M.Name = "textBox_M";
            this.textBox_M.Size = new System.Drawing.Size(100, 19);
            this.textBox_M.TabIndex = 0;
            this.textBox_M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "科目";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "数学";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "物理";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "英語";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(96, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 5;
            this.label5.Text = "出席率";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(244, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "得点";
            // 
            // textBoxScore_M
            // 
            this.textBoxScore_M.Location = new System.Drawing.Point(246, 76);
            this.textBoxScore_M.Name = "textBoxScore_M";
            this.textBoxScore_M.Size = new System.Drawing.Size(100, 19);
            this.textBoxScore_M.TabIndex = 1;
            this.textBoxScore_M.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxScore_P
            // 
            this.textBoxScore_P.Location = new System.Drawing.Point(246, 123);
            this.textBoxScore_P.Name = "textBoxScore_P";
            this.textBoxScore_P.Size = new System.Drawing.Size(100, 19);
            this.textBoxScore_P.TabIndex = 3;
            this.textBoxScore_P.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxScore_E
            // 
            this.textBoxScore_E.Location = new System.Drawing.Point(246, 176);
            this.textBoxScore_E.Name = "textBoxScore_E";
            this.textBoxScore_E.Size = new System.Drawing.Size(100, 19);
            this.textBoxScore_E.TabIndex = 5;
            this.textBoxScore_E.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_E
            // 
            this.textBox_E.Location = new System.Drawing.Point(96, 177);
            this.textBox_E.Name = "textBox_E";
            this.textBox_E.Size = new System.Drawing.Size(100, 19);
            this.textBox_E.TabIndex = 4;
            this.textBox_E.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_P
            // 
            this.textBox_P.Location = new System.Drawing.Point(96, 124);
            this.textBox_P.Name = "textBox_P";
            this.textBox_P.Size = new System.Drawing.Size(100, 19);
            this.textBox_P.TabIndex = 2;
            this.textBox_P.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(389, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 13;
            this.label7.Text = "判定結果";
            // 
            // labelResult_M
            // 
            this.labelResult_M.AutoSize = true;
            this.labelResult_M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelResult_M.Location = new System.Drawing.Point(389, 79);
            this.labelResult_M.Name = "labelResult_M";
            this.labelResult_M.Size = new System.Drawing.Size(52, 16);
            this.labelResult_M.TabIndex = 14;
            this.labelResult_M.Text = "label8";
            // 
            // labelResult_P
            // 
            this.labelResult_P.AutoSize = true;
            this.labelResult_P.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelResult_P.Location = new System.Drawing.Point(389, 126);
            this.labelResult_P.Name = "labelResult_P";
            this.labelResult_P.Size = new System.Drawing.Size(52, 16);
            this.labelResult_P.TabIndex = 15;
            this.labelResult_P.Text = "label9";
            // 
            // labelResult_E
            // 
            this.labelResult_E.AutoSize = true;
            this.labelResult_E.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelResult_E.Location = new System.Drawing.Point(389, 179);
            this.labelResult_E.Name = "labelResult_E";
            this.labelResult_E.Size = new System.Drawing.Size(61, 16);
            this.labelResult_E.TabIndex = 16;
            this.labelResult_E.Text = "label10";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(495, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 12);
            this.label11.TabIndex = 17;
            this.label11.Text = "平均点と比較";
            // 
            // labelConmpAvg_M
            // 
            this.labelConmpAvg_M.AutoSize = true;
            this.labelConmpAvg_M.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelConmpAvg_M.Location = new System.Drawing.Point(495, 79);
            this.labelConmpAvg_M.Name = "labelConmpAvg_M";
            this.labelConmpAvg_M.Size = new System.Drawing.Size(61, 16);
            this.labelConmpAvg_M.TabIndex = 18;
            this.labelConmpAvg_M.Text = "label12";
            // 
            // labelConmpAvg_P
            // 
            this.labelConmpAvg_P.AutoSize = true;
            this.labelConmpAvg_P.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelConmpAvg_P.Location = new System.Drawing.Point(495, 123);
            this.labelConmpAvg_P.Name = "labelConmpAvg_P";
            this.labelConmpAvg_P.Size = new System.Drawing.Size(61, 16);
            this.labelConmpAvg_P.TabIndex = 19;
            this.labelConmpAvg_P.Text = "label13";
            // 
            // labelConmpAvg_E
            // 
            this.labelConmpAvg_E.AutoSize = true;
            this.labelConmpAvg_E.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelConmpAvg_E.Location = new System.Drawing.Point(495, 179);
            this.labelConmpAvg_E.Name = "labelConmpAvg_E";
            this.labelConmpAvg_E.Size = new System.Drawing.Size(61, 16);
            this.labelConmpAvg_E.TabIndex = 20;
            this.labelConmpAvg_E.Text = "label14";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(202, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 12);
            this.label15.TabIndex = 21;
            this.label15.Text = "%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(202, 127);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(11, 12);
            this.label16.TabIndex = 22;
            this.label16.Text = "%";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(202, 180);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 12);
            this.label17.TabIndex = 23;
            this.label17.Text = "%";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 236);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "判定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(373, 236);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "リセット";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 292);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.labelConmpAvg_E);
            this.Controls.Add(this.labelConmpAvg_P);
            this.Controls.Add(this.labelConmpAvg_M);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.labelResult_E);
            this.Controls.Add(this.labelResult_P);
            this.Controls.Add(this.labelResult_M);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox_P);
            this.Controls.Add(this.textBox_E);
            this.Controls.Add(this.textBoxScore_E);
            this.Controls.Add(this.textBoxScore_P);
            this.Controls.Add(this.textBoxScore_M);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_M);
            this.Name = "Form1";
            this.Text = "成績判定";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_M;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxScore_M;
        private System.Windows.Forms.TextBox textBoxScore_P;
        private System.Windows.Forms.TextBox textBoxScore_E;
        private System.Windows.Forms.TextBox textBox_E;
        private System.Windows.Forms.TextBox textBox_P;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelResult_M;
        private System.Windows.Forms.Label labelResult_P;
        private System.Windows.Forms.Label labelResult_E;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label labelConmpAvg_M;
        private System.Windows.Forms.Label labelConmpAvg_P;
        private System.Windows.Forms.Label labelConmpAvg_E;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}


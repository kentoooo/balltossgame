﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace seiseki
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int nationallanguage, math, science, society, english,sum,ave;
            double aveW;
            string rank;

            nationallanguage = int.Parse(textBox1.Text);
            math = int.Parse(textBox2.Text);
            science = int.Parse(textBox3.Text);
            society = int.Parse(textBox4.Text);
            english = int.Parse(textBox5.Text);

            sum = nationallanguage + math + science + society + english;
            aveW = sum / 5;
            ave = sum / 5;

            if(aveW-ave>=0.5)
            {
                ave += 1;
            }



            if (aveW == 100)
            {
                rank = "S";
            }
            else if(aveW>=80)
            {
                rank = "A";
            }
            else if(aveW>=65)
            {
                rank = "B";
            }
            else if(aveW>=50)
            {
                rank = "C";
            }
            else if(aveW>=40)
            {
                rank = "D";
            }
            else if(aveW>=0)
            {
                rank = "E";
            }
            else
            {
                rank = "エラー";
            }

            SUM.Text="合計"+sum;
            AVE.Text = "平均"+ave;
            RANKING.Text = rank;
        }
    }
}

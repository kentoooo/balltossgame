﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AddTax
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int money;
            double addTax;
            const double tax = 0.1;

            money = int.Parse(textBox1.Text);

            addTax = money;
            addTax *= (1 + tax);
            money = (int)addTax;

            label4.Text = money + "円";
        }
    }
}

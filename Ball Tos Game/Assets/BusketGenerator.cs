﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusketGenerator : MonoBehaviour
{
    public GameObject BusketPrefab;
    float span = 1.0f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(BusketPrefab) as GameObject;
        }
    }                                
}

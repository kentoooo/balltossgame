﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    public static int Co = 0;
    public static float time = 1800.0f;

    public GameObject BallCount = null;
    GameObject timer;

    // Start is called before the first frame update
    void Start()
    {
        this.timer = GameObject.Find("timer");

        time = 30.0f;
        Co = 0;
    }

    public void DecreaseCo()
    {
        Co++;
    }
   
    // Update is called once per frame
    void Update()
    {
        Text arrowText = this.BallCount.GetComponent<Text>();
        arrowText.text = Co + "個";

        time -= Time.deltaTime;
        this.timer.GetComponent<Image>().fillAmount -= 0.00056f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public GameObject BallPrefab;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //左矢印キーが押されたなら
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, 0, 5);
        }
        //右矢印キーが押されたなら
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, 0, -5);
        }
        //スペースキーが押されたなら
        if(Input.GetKeyDown(KeyCode.Space))
        {
            GameObject go = Instantiate(BallPrefab) as GameObject;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{
    public GameObject BallPrefab;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float time = GameDirector.time;

        if (time > 0)
        {
            //もしスペースキーが押されたなら
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //（インスタンス化とは、オブジェクト指向のプログラミングにおいて、インスタンス（クラスを基にした実際の値としてのデータ）を生成すること）
                GameObject ball = Instantiate(BallPrefab) as GameObject;
                ball.transform.position = transform.position;
                ball.transform.rotation = transform.rotation;
            }

        }
    }
}


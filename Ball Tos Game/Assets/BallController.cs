﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    //ボールにセットされている物理法則を制御するコンポーネントを入れる変数
    Rigidbody2D rigit2D;

    //ボールを飛ばす力
    float BoundForse = 280.0f;
    
    
    // Start is called before the first frame update
    void Start()
    {
        //ボールにセットされている物理法則を制御するコンポーネントを入れる
        this.rigit2D = GetComponent<Rigidbody2D>();

        //自分から見て上方向にBoundForse分の力を加える
        this.rigit2D.AddForce(transform.up * this.BoundForse);
    }

    // Update is called once per frame
    void Update()
    {
        //もし-10以上の座標になったら
        if(transform.position.y<-10.0f)
        {
            //ゲームオブジェクトを破壊する
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        //ゴールと表示し、すべてのゲームオブジェクトを破壊する
        Debug.Log("ゴール");
        Destroy(gameObject);

        GameObject Director = GameObject.Find("GameDirector");
        Director.GetComponent<GameDirector>().DecreaseCo();
    }
}
